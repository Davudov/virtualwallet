package com.example.app.entity

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.app.BR
import java.text.DateFormat
import java.util.*


@Entity
class Card(
    var name: String,
    var amount: Double,
    var createdTime: Long,
    var currency: String = "AZN",
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
){
    fun getAmountString():String{
        return amount.toString()
    }

    fun getCreatedTimeString():String{
        val date = Date(createdTime)
        val formatter = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault())
        return formatter.format(date)
    }
}

class CardDetails() : BaseObservable() {
    @Bindable
    var name: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.name)
        }
        get() = field

    @Bindable
    var amount: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.amount)
        }
        get() = field

}
