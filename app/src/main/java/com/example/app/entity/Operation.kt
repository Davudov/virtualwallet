package com.example.app.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Operation(
    var amount: Double,
    var time: Long,
    @PrimaryKey(autoGenerate = true)
    var id: Int
)