package com.example.app.persistance.card

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.example.app.entity.Card

@Dao
interface CardDao {
    @Insert
     fun insertCard(card: Card)

    @Query("select * from Card")
     fun getCards(): LiveData<List<Card>>

    @Delete
    suspend fun removeCards(cardList: List<Card>)
}
