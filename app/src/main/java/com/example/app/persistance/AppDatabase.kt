package com.example.app.persistance

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.app.entity.Card
import com.example.app.persistance.card.CardDao

@Database(entities = arrayOf(Card::class),version = 2,exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getCardDao(): CardDao

    companion object {
        fun provideDatabase(application: Application): AppDatabase =
            Room.databaseBuilder(application, AppDatabase::class.java, "database")
                .fallbackToDestructiveMigration()
                .build()
    }

}
