package com.example.app

import android.app.Application
import com.example.app.persistance.AppDatabase.Companion.provideDatabase
import com.example.app.repository.CardRepo
import com.example.app.ui.add_card.AddCardViewModel
import com.example.app.ui.card.CardViewModel
import com.example.app.ui.operation.OperationsViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import timber.log.Timber

class MApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
        initTimber()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initKoin() {
        val moduleDefinition = module {

            viewModel {
                CardViewModel()
            }

            viewModel {
                AddCardViewModel()
            }
            viewModel { OperationsViewModel() }

            single {
                provideDatabase(get())
            }
            single { CardRepo(get()) }
        }
        startKoin {
            androidContext(this@MApplication)
            modules(listOf(moduleDefinition))
        }
    }
}