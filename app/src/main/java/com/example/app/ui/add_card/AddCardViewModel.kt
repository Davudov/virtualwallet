package com.example.app.ui.add_card

import android.text.TextWatcher
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.app.entity.Card
import com.example.app.entity.CardDetails
import com.example.app.persistance.card.CardDao
import com.example.app.repository.CardRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import org.koin.core.KoinComponent
import org.koin.core.inject

class AddCardViewModel : ViewModel(), KoinComponent {
    private val cardRepo: CardRepo by inject()

    fun saveCard(cardDetails: Card) {
        runBlocking {
            withContext(Dispatchers.IO){
                cardRepo.insertCard(cardDetails)
            }
        }
    }

    val name = ObservableField<String>()

    val amount = MutableLiveData<String>()



}