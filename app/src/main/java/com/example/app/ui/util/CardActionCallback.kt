package com.example.app.ui.util

interface CardActionCallback {
    fun onDeleteItem()
    fun onDestroyActionMode()
}