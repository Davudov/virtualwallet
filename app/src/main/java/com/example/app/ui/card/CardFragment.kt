package com.example.app.ui.card

import android.os.Bundle
import android.view.ActionMode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.app.R
import com.example.app.base.ItemClickListener
import com.example.app.base.ItemLongClickListener
import com.example.app.base.util.createAlert
import com.example.app.databinding.FragmentCardBinding
import com.example.app.ui.util.CardActionCallback
import com.example.app.ui.util.CardActionMode
import kotlinx.coroutines.runBlocking
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class CardFragment : Fragment(), ItemLongClickListener, ItemClickListener, CardActionCallback {
    private lateinit var cardBinding: FragmentCardBinding
    val viewModel: CardViewModel by viewModel()
    private var actionMode: ActionMode? = null
    private lateinit var cardAdapter: CardAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        cardBinding = FragmentCardBinding.inflate(inflater)
        cardBinding.viewModel = viewModel
        cardBinding.lifecycleOwner = this
        initViews()
        return cardBinding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (actionMode != null) {
            actionMode?.finish()
            actionMode = null
        }
    }

    private fun initViews() {
        cardBinding.floatingActionButton.setOnClickListener { findNavController().navigate(R.id.addCardFragment) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardAdapter =
            CardAdapter(this@CardFragment.requireContext(), this@CardFragment, this@CardFragment)

        cardBinding.cardList.apply {
            adapter = cardAdapter
        }


        viewModel.getCards().observe(viewLifecycleOwner, Observer {
            if (it != null)
                cardAdapter.addAll(it)
        })

    }

    override fun onLongClick(view: View, position: Int): Boolean {
        Timber.d("item long clicked")

        if (actionMode != null) {
            return false
        }
        actionMode = requireActivity().startActionMode(CardActionMode(this))
        actionMode?.title = "1 selected"
        toggleSelection(position)
        return true
    }

    override fun onClick(view: View, position: Int) {
        Timber.d("item clicked")
        if (actionMode != null) {
            toggleSelection(position)
        }
    }

    private fun toggleSelection(position: Int) {
        cardAdapter.toggleSelection(position)
        val count = cardAdapter.getSelectedItemCount()
        if (count == 0) {
            actionMode?.finish()
            actionMode = null
        } else {
            actionMode?.title = count.toString()
            actionMode?.invalidate()
        }
    }

    override fun onDeleteItem() {
        requireContext().createAlert {
            setMessage("Are you sure to delete this card ?")
            setButton(AlertDialog.BUTTON_NEGATIVE, "NO") { _, _ ->
                dismiss()
            }
            setButton(AlertDialog.BUTTON_POSITIVE, "Yes") { _, _ ->
                cardAdapter.clearSelectedItems {
                    runBlocking {
                        viewModel.deleteCards(it)
                    }
                }
                actionMode?.finish()
                actionMode = null
            }
            show()
        }
    }

    override fun onDestroyActionMode() {
        actionMode = null
        cardAdapter.clearSelections()
    }


}

