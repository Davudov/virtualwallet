package com.example.app.ui.util

import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import com.example.app.R

class CardActionMode(private val cardActionCallback: CardActionCallback): ActionMode.Callback {
    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        mode?:return false
        mode.menuInflater.inflate(R.menu.card_actions_menu, menu)
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        return false
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        item?:return false
        when (item.itemId) {
            R.id.card_action_delete -> {
                cardActionCallback.onDeleteItem()
            }
        }
        return true
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        cardActionCallback.onDestroyActionMode()
    }
}