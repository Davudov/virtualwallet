package com.example.app.ui.operation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.app.R
import com.example.app.databinding.FragmentOperationBinding

class OperationsFragment :Fragment(R.layout.fragment_operation){
    private lateinit var binding:FragmentOperationBinding

    private val navController: NavController by lazy {
        findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOperationBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.addOperationBtn.setOnClickListener {
            navigateToSelectOp()
        }
    }

    private fun navigateToSelectOp() {
        navController.navigate(R.id.selectFragment)
    }



}