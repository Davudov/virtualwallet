package com.example.app.ui.add_card

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.app.databinding.FragmentAddCardBinding
import com.example.app.entity.Card
import com.example.app.entity.CardDetails
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddCardFragment : Fragment() {
    val viewModel: AddCardViewModel by viewModel()
    private lateinit var binding: FragmentAddCardBinding

    private val cardDetails = CardDetails()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentAddCardBinding.inflate(inflater)
        binding.details = cardDetails
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.button.setOnClickListener {
            if (binding.nameTv.text.isEmpty()) {
                binding.nameTv.error = "Enter card name"
                return@setOnClickListener
            }
            else if (binding.amountTv.text.isEmpty()) {
                binding.amountTv.error = "Enter card amount"
                return@setOnClickListener
            }
            viewModel.saveCard(
                Card(
                    cardDetails.name!!,
                    cardDetails.amount?.toDouble()!!,
                    System.currentTimeMillis()
                )
            )
        }
    }
}
