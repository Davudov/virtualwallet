package com.example.app.ui.card

import android.content.Context
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.example.app.R
import com.example.app.base.BaseAdapter
import com.example.app.base.ItemClickListener
import com.example.app.base.ItemLongClickListener
import com.example.app.databinding.CardListItemBinding
import com.example.app.entity.Card

class CardAdapter(
    context: Context, itemClickListener: ItemClickListener,
    itemLongClickListener: ItemLongClickListener
) : BaseAdapter<Card, CardAdapter.ViewHolder>(context, itemClickListener, itemLongClickListener) {


    fun getSelectedItemCount():Int = selectedPositions.size()


    fun clearSelections(){
        selectedPositions.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CardListItemBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    fun toggleSelection(position: Int) {
        if (selectedPositions.get(position, false)) { //if chosen
            selectedPositions.delete(position)
        }else{
            selectedPositions.put(position,true) //if not chosen
        }
        notifyItemChanged(position)
    }

    inner class ViewHolder(
        private val binding: CardListItemBinding,
    ) : BaseAdapter.Holder<Card>(binding.root), View.OnLongClickListener, View.OnClickListener {

        override fun unbind() {

        }

        override fun bind(item: Card) {
            binding.parentCardView.isActivated = selectedPositions[adapterPosition]
            if (selectedPositions[adapterPosition]) binding.parentCardView.setBackgroundColor(ContextCompat.getColor(context,
                R.color.colorSelectedCard))
            else binding.parentCardView.setBackgroundColor(ContextCompat.getColor(context,R.color.colorUnSelectedCard))
            binding.parentCardView.setOnLongClickListener(this)
            binding.parentCardView.setOnClickListener(this)
            binding.card = item
            binding.executePendingBindings()
            super.bind(item)
        }

        override fun onLongClick(v: View?): Boolean {
            v?.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY,HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING)
            return longClickListener?.onLongClick(v!!, adapterPosition)!!
        }

        override fun onClick(v: View?) {
            itemClickListener?.onClick(v!!, adapterPosition)
        }
    }

}

