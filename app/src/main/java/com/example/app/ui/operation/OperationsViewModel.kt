package com.example.app.ui.operation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class OperationsViewModel : ViewModel() {
    private val _navigateTo = MutableLiveData<OperationsNav>()
    val navigateTo: LiveData<OperationsNav> get() = _navigateTo

    fun onIncreaseClicked() {
        _navigateTo.postValue(OperationsNav.NAVIGATE_TO_INCREASE)
    }
    fun onDecreaseClicked() {
        _navigateTo.postValue(OperationsNav.NAVIGATE_TO_DECREASE)
    }

    enum class OperationsNav {
        NAVIGATE_TO_INCREASE,
        NAVIGATE_TO_DECREASE
    }
}