package com.example.app.ui.operation.select_op

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.app.R
import com.example.app.databinding.FragmentSelectBinding
import com.example.app.ui.operation.OperationsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class SelectOperationFragment :Fragment(R.layout.fragment_select){
    private val viewModel:OperationsViewModel by viewModel()
private lateinit var binding :FragmentSelectBinding

    private val navController: NavController by lazy {
        findNavController()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSelectBinding.inflate(inflater)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.navigateTo.observe(viewLifecycleOwner, Observer {
//            when(it){
//                OperationsViewModel.OperationsNav.NAVIGATE_TO_DECREASE -> navController.navigate()
//                OperationsViewModel.OperationsNav.NAVIGATE_TO_INCREASE -> navController.navigate()
//            }
        })
    }

}