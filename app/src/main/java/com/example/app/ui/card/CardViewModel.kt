package com.example.app.ui.card

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.app.entity.Card
import com.example.app.repository.CardRepo
import org.koin.core.KoinComponent
import org.koin.core.inject


class CardViewModel():ViewModel() ,KoinComponent{
//     val cards = MutableLiveData<List<Card>>()
    private val cardRepo:CardRepo by inject()

    fun getCards(): LiveData<List<Card>> {
        return cardRepo.getCards()
    }

    suspend fun deleteCards(cardList:List<Card>) {
        cardRepo.deleteCards(cardList)
    }

}