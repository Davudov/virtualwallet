package com.example.app.base

import android.content.Context
import android.util.SparseBooleanArray
import android.view.View
import androidx.core.util.forEach
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<E, Holder : BaseAdapter.Holder<E>> : RecyclerView.Adapter<Holder> {
    var context: Context

    var items: MutableList<E>

    var backup: List<E>

    var itemClickListener: ItemClickListener? = null

    var longClickListener: ItemLongClickListener? = null

    val selectedPositions: SparseBooleanArray = SparseBooleanArray()

    internal constructor(context: Context) {
        this.context = context
        items = ArrayList()
        backup = ArrayList()
        itemClickListener = null
        longClickListener = null
    }

    internal constructor(context: Context, itemClickListener: ItemClickListener) {
        this.context = context
        items = ArrayList()
        backup = ArrayList()
        this.itemClickListener = itemClickListener
    }

    internal constructor(context: Context, itemLongClickListener: ItemLongClickListener) {
        this.context = context
        items = ArrayList()
        backup = ArrayList()
        longClickListener = itemLongClickListener
    }

    constructor(context: Context, itemClickListener: ItemClickListener, longClickListener: ItemLongClickListener) {
        this.context = context
        items = ArrayList()
        backup = ArrayList()
        this.itemClickListener = itemClickListener
        this.longClickListener = longClickListener
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItemAt(position))
    }

    override fun onViewRecycled(holder: Holder) {
        holder.unbind()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    val backupItemsCount: Int
        get() = backup.size

    fun getItemAt(position: Int): E {
        return items[position]
    }

    fun removeItemAt(position: Int) {
        items.removeAt(position)
        this.backup = items
        notifyItemRemoved(position)
    }

    fun clear() {
        items.clear()
        this.backup = items
        notifyDataSetChanged()
    }

    fun clearItem(position: Int) {
        items.removeAt(position)
        this.backup = items
        notifyDataSetChanged()
    }

    fun clearSelectedItems(getSelectedItems:(List<E>)->Unit){

        val selectedItems = arrayListOf<E>()
        selectedPositions.forEach { key, value ->
            selectedItems.add(items[key])
        }
        getSelectedItems(selectedItems)


        val hashMap = HashMap<Int,E>()
        items.forEachIndexed{index,p->
            hashMap[index] = p
        }
        selectedPositions.forEach{position,_->
            hashMap.remove(position)
        }

        items.clear()
        hashMap.entries.forEach {
            items.add(it.value)
        }
        this.backup = items
        notifyDataSetChanged()
    }

    fun restoreItem(deleteItem: E, position: Int) {
        items.add(position, deleteItem)
        this.backup = items
        notifyItemInserted(position)
    }

    fun add(item: E) {
        items.add(item)
        this.backup = items
        notifyItemInserted(itemCount - 1)
    }

    fun addFirst(item: E) {
        items.add(0, item)
        this.backup = items
        notifyItemInserted(0)
    }

    fun addAll(items: List<E>) {
        this.items.clear()
        this.items.addAll(items)
        this.backup = this.items
        notifyDataSetChanged()
    }

    fun addAllNoClear(items: List<E>) {
        this.items.addAll(items)
        this.backup = this.items
        notifyDataSetChanged()
    }

    fun updateItem(position: Int, item: E) {
        items[position] = item
        backup = items
        notifyItemChanged(position)
    }
//
//    fun updateVerticalItem(horizoontal: Int, vertical: Int) {
////        notifyItemChanged(horizoontal);
//    }

    fun getItemAtPosition(position: Int): E {
        return items[position]
    }


//
//    fun clearAll(items: List<E>) {
//        this.items.clear()
//    }

    abstract class Holder<E> constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        open fun bind(item: E) {}
        abstract fun unbind()
    }
}
