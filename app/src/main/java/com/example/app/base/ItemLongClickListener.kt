package com.example.app.base

import android.view.View

interface ItemLongClickListener {
    fun onLongClick(view: View,position:Int):Boolean
}