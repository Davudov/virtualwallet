package com.example.app.base

import android.view.View

interface ItemClickListener {
    fun onClick(view: View, position:Int)
}