package com.example.app.base.util

import android.app.AlertDialog
import android.content.Context
import android.widget.Toast


inline fun Context.createAlert(onAlert: AlertDialog.() -> Unit) :AlertDialog{
    val alertDialog = AlertDialog.Builder(this).create()
    alertDialog.apply(onAlert)
    return alertDialog
}

fun Context.makeToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}