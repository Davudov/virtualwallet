package com.example.app.repository

import androidx.lifecycle.LiveData
import com.example.app.entity.Card
import com.example.app.persistance.AppDatabase

class CardRepo (val database: AppDatabase){
     fun insertCard(card:Card){
        //send created date to cloud
        database.getCardDao().insertCard(card)
    }

    fun getCards(): LiveData<List<Card>> {
        return database.getCardDao().getCards()
    }

    suspend fun deleteCards(cardList: List<Card>) {
        return database.getCardDao().removeCards(cardList)
    }
}