// Generated by data binding compiler. Do not edit!
package com.example.app.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.example.app.R;
import com.example.app.ui.card.CardViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentCardBinding extends ViewDataBinding {
  @NonNull
  public final RecyclerView cardList;

  @NonNull
  public final FloatingActionButton floatingActionButton;

  @Bindable
  protected CardViewModel mViewModel;

  protected FragmentCardBinding(Object _bindingComponent, View _root, int _localFieldCount,
      RecyclerView cardList, FloatingActionButton floatingActionButton) {
    super(_bindingComponent, _root, _localFieldCount);
    this.cardList = cardList;
    this.floatingActionButton = floatingActionButton;
  }

  public abstract void setViewModel(@Nullable CardViewModel viewModel);

  @Nullable
  public CardViewModel getViewModel() {
    return mViewModel;
  }

  @NonNull
  public static FragmentCardBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_card, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentCardBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentCardBinding>inflateInternal(inflater, R.layout.fragment_card, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentCardBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_card, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentCardBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentCardBinding>inflateInternal(inflater, R.layout.fragment_card, null, false, component);
  }

  public static FragmentCardBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentCardBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentCardBinding)bind(component, view, R.layout.fragment_card);
  }
}
