package com.example.app;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.example.app.databinding.CardListItemBindingImpl;
import com.example.app.databinding.FragmentAddCardBindingImpl;
import com.example.app.databinding.FragmentCardBindingImpl;
import com.example.app.databinding.FragmentOperationBindingImpl;
import com.example.app.databinding.FragmentSelectBindingImpl;
import com.example.app.databinding.FragmentSelectDetailsBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_CARDLISTITEM = 1;

  private static final int LAYOUT_FRAGMENTADDCARD = 2;

  private static final int LAYOUT_FRAGMENTCARD = 3;

  private static final int LAYOUT_FRAGMENTOPERATION = 4;

  private static final int LAYOUT_FRAGMENTSELECT = 5;

  private static final int LAYOUT_FRAGMENTSELECTDETAILS = 6;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(6);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.app.R.layout.card_list_item, LAYOUT_CARDLISTITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.app.R.layout.fragment_add_card, LAYOUT_FRAGMENTADDCARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.app.R.layout.fragment_card, LAYOUT_FRAGMENTCARD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.app.R.layout.fragment_operation, LAYOUT_FRAGMENTOPERATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.app.R.layout.fragment_select, LAYOUT_FRAGMENTSELECT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.app.R.layout.fragment_select_details, LAYOUT_FRAGMENTSELECTDETAILS);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_CARDLISTITEM: {
          if ("layout/card_list_item_0".equals(tag)) {
            return new CardListItemBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for card_list_item is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTADDCARD: {
          if ("layout/fragment_add_card_0".equals(tag)) {
            return new FragmentAddCardBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_add_card is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCARD: {
          if ("layout/fragment_card_0".equals(tag)) {
            return new FragmentCardBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_card is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTOPERATION: {
          if ("layout/fragment_operation_0".equals(tag)) {
            return new FragmentOperationBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_operation is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSELECT: {
          if ("layout/fragment_select_0".equals(tag)) {
            return new FragmentSelectBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_select is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSELECTDETAILS: {
          if ("layout/fragment_select_details_0".equals(tag)) {
            return new FragmentSelectDetailsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_select_details is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(6);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "amount");
      sKeys.put(2, "card");
      sKeys.put(3, "details");
      sKeys.put(4, "name");
      sKeys.put(5, "viewModel");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(6);

    static {
      sKeys.put("layout/card_list_item_0", com.example.app.R.layout.card_list_item);
      sKeys.put("layout/fragment_add_card_0", com.example.app.R.layout.fragment_add_card);
      sKeys.put("layout/fragment_card_0", com.example.app.R.layout.fragment_card);
      sKeys.put("layout/fragment_operation_0", com.example.app.R.layout.fragment_operation);
      sKeys.put("layout/fragment_select_0", com.example.app.R.layout.fragment_select);
      sKeys.put("layout/fragment_select_details_0", com.example.app.R.layout.fragment_select_details);
    }
  }
}
