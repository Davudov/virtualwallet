package com.example.app.databinding;
import com.example.app.R;
import com.example.app.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class CardListItemBindingImpl extends CardListItemBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public CardListItemBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private CardListItemBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[3]
            , (androidx.cardview.widget.CardView) bindings[0]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.amountTv.setTag(null);
        this.parentCardView.setTag(null);
        this.textView3.setTag(null);
        this.textView7.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.card == variableId) {
            setCard((com.example.app.entity.Card) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setCard(@Nullable com.example.app.entity.Card Card) {
        this.mCard = Card;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.card);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String cardGetAmountString = null;
        com.example.app.entity.Card card = mCard;
        java.lang.String textView7AndroidStringTimeCardGetCreatedTimeString = null;
        java.lang.String amountTvAndroidStringAmountManatCardGetAmountString = null;
        java.lang.String cardGetCreatedTimeString = null;
        java.lang.String cardName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (card != null) {
                    // read card.getAmountString()
                    cardGetAmountString = card.getAmountString();
                    // read card.getCreatedTimeString()
                    cardGetCreatedTimeString = card.getCreatedTimeString();
                    // read card.name
                    cardName = card.getName();
                }


                // read @android:string/amount_manat
                amountTvAndroidStringAmountManatCardGetAmountString = amountTv.getResources().getString(R.string.amount_manat, cardGetAmountString);
                // read @android:string/time
                textView7AndroidStringTimeCardGetCreatedTimeString = textView7.getResources().getString(R.string.time, cardGetCreatedTimeString);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.amountTv, amountTvAndroidStringAmountManatCardGetAmountString);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView3, cardName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView7, textView7AndroidStringTimeCardGetCreatedTimeString);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): card
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}