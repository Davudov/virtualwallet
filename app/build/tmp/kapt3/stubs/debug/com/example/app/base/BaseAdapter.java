package com.example.app.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0010!\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0010\b&\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u000e\b\u0001\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00010\u00032\b\u0012\u0004\u0012\u0002H\u00020\u0004:\u0001\u0002B\u000f\b\u0010\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u0017\b\u0010\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nB\u0017\b\u0010\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u000b\u001a\u00020\f\u00a2\u0006\u0002\u0010\rB\u001f\b\u0016\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\u000e\u001a\u00020\f\u00a2\u0006\u0002\u0010\u000fJ\u0013\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u00028\u0000\u00a2\u0006\u0002\u00100J\u0014\u00101\u001a\u00020.2\f\u0010!\u001a\b\u0012\u0004\u0012\u00028\u00000\u0011J\u0014\u00102\u001a\u00020.2\f\u0010!\u001a\b\u0012\u0004\u0012\u00028\u00000\u0011J\u0013\u00103\u001a\u00020.2\u0006\u0010/\u001a\u00028\u0000\u00a2\u0006\u0002\u00100J\u0006\u00104\u001a\u00020.J\u000e\u00105\u001a\u00020.2\u0006\u00106\u001a\u00020\u0017J \u00107\u001a\u00020.2\u0018\u00108\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0011\u0012\u0004\u0012\u00020.09J\u0013\u0010:\u001a\u00028\u00002\u0006\u00106\u001a\u00020\u0017\u00a2\u0006\u0002\u0010;J\u0013\u0010<\u001a\u00028\u00002\u0006\u00106\u001a\u00020\u0017\u00a2\u0006\u0002\u0010;J\b\u0010=\u001a\u00020\u0017H\u0016J\u001d\u0010>\u001a\u00020.2\u0006\u0010?\u001a\u00028\u00012\u0006\u00106\u001a\u00020\u0017H\u0016\u00a2\u0006\u0002\u0010@J\u0015\u0010A\u001a\u00020.2\u0006\u0010?\u001a\u00028\u0001H\u0016\u00a2\u0006\u0002\u0010BJ\u000e\u0010C\u001a\u00020.2\u0006\u00106\u001a\u00020\u0017J\u001b\u0010D\u001a\u00020.2\u0006\u0010E\u001a\u00028\u00002\u0006\u00106\u001a\u00020\u0017\u00a2\u0006\u0002\u0010FJ\u001b\u0010G\u001a\u00020.2\u0006\u00106\u001a\u00020\u00172\u0006\u0010/\u001a\u00028\u0000\u00a2\u0006\u0002\u0010HR \u0010\u0010\u001a\b\u0012\u0004\u0012\u00028\u00000\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0016\u001a\u00020\u00178F\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u0007R\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R \u0010!\u001a\b\u0012\u0004\u0012\u00028\u00000\"X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u0013\"\u0004\b$\u0010\u0015R\u001c\u0010\u000e\u001a\u0004\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010&\"\u0004\b\'\u0010(R\u0011\u0010)\u001a\u00020*\u00a2\u0006\b\n\u0000\u001a\u0004\b+\u0010,\u00a8\u0006I"}, d2 = {"Lcom/example/app/base/BaseAdapter;", "E", "Holder", "Lcom/example/app/base/BaseAdapter$Holder;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "itemClickListener", "Lcom/example/app/base/ItemClickListener;", "(Landroid/content/Context;Lcom/example/app/base/ItemClickListener;)V", "itemLongClickListener", "Lcom/example/app/base/ItemLongClickListener;", "(Landroid/content/Context;Lcom/example/app/base/ItemLongClickListener;)V", "longClickListener", "(Landroid/content/Context;Lcom/example/app/base/ItemClickListener;Lcom/example/app/base/ItemLongClickListener;)V", "backup", "", "getBackup", "()Ljava/util/List;", "setBackup", "(Ljava/util/List;)V", "backupItemsCount", "", "getBackupItemsCount", "()I", "getContext", "()Landroid/content/Context;", "setContext", "getItemClickListener", "()Lcom/example/app/base/ItemClickListener;", "setItemClickListener", "(Lcom/example/app/base/ItemClickListener;)V", "items", "", "getItems", "setItems", "getLongClickListener", "()Lcom/example/app/base/ItemLongClickListener;", "setLongClickListener", "(Lcom/example/app/base/ItemLongClickListener;)V", "selectedPositions", "Landroid/util/SparseBooleanArray;", "getSelectedPositions", "()Landroid/util/SparseBooleanArray;", "add", "", "item", "(Ljava/lang/Object;)V", "addAll", "addAllNoClear", "addFirst", "clear", "clearItem", "position", "clearSelectedItems", "getSelectedItems", "Lkotlin/Function1;", "getItemAt", "(I)Ljava/lang/Object;", "getItemAtPosition", "getItemCount", "onBindViewHolder", "holder", "(Lcom/example/app/base/BaseAdapter$Holder;I)V", "onViewRecycled", "(Lcom/example/app/base/BaseAdapter$Holder;)V", "removeItemAt", "restoreItem", "deleteItem", "(Ljava/lang/Object;I)V", "updateItem", "(ILjava/lang/Object;)V", "app_debug"})
public abstract class BaseAdapter<E extends java.lang.Object, Holder extends com.example.app.base.BaseAdapter.Holder<E>> extends androidx.recyclerview.widget.RecyclerView.Adapter<Holder> {
    @org.jetbrains.annotations.NotNull()
    private android.content.Context context;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<E> items;
    @org.jetbrains.annotations.NotNull()
    private java.util.List<? extends E> backup;
    @org.jetbrains.annotations.Nullable()
    private com.example.app.base.ItemClickListener itemClickListener;
    @org.jetbrains.annotations.Nullable()
    private com.example.app.base.ItemLongClickListener longClickListener;
    @org.jetbrains.annotations.NotNull()
    private final android.util.SparseBooleanArray selectedPositions = null;
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public final void setContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<E> getItems() {
        return null;
    }
    
    public final void setItems(@org.jetbrains.annotations.NotNull()
    java.util.List<E> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<E> getBackup() {
        return null;
    }
    
    public final void setBackup(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends E> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.app.base.ItemClickListener getItemClickListener() {
        return null;
    }
    
    public final void setItemClickListener(@org.jetbrains.annotations.Nullable()
    com.example.app.base.ItemClickListener p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.app.base.ItemLongClickListener getLongClickListener() {
        return null;
    }
    
    public final void setLongClickListener(@org.jetbrains.annotations.Nullable()
    com.example.app.base.ItemLongClickListener p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.util.SparseBooleanArray getSelectedPositions() {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    Holder holder, int position) {
    }
    
    @java.lang.Override()
    public void onViewRecycled(@org.jetbrains.annotations.NotNull()
    Holder holder) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final int getBackupItemsCount() {
        return 0;
    }
    
    public final E getItemAt(int position) {
        return null;
    }
    
    public final void removeItemAt(int position) {
    }
    
    public final void clear() {
    }
    
    public final void clearItem(int position) {
    }
    
    public final void clearSelectedItems(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.util.List<? extends E>, kotlin.Unit> getSelectedItems) {
    }
    
    public final void restoreItem(E deleteItem, int position) {
    }
    
    public final void add(E item) {
    }
    
    public final void addFirst(E item) {
    }
    
    public final void addAll(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends E> items) {
    }
    
    public final void addAllNoClear(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends E> items) {
    }
    
    public final void updateItem(int position, E item) {
    }
    
    public final E getItemAtPosition(int position) {
        return null;
    }
    
    public BaseAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    public BaseAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.example.app.base.ItemClickListener itemClickListener) {
        super();
    }
    
    public BaseAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.example.app.base.ItemLongClickListener itemLongClickListener) {
        super();
    }
    
    public BaseAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.example.app.base.ItemClickListener itemClickListener, @org.jetbrains.annotations.NotNull()
    com.example.app.base.ItemLongClickListener longClickListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b&\u0018\u0000*\u0004\b\u0002\u0010\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0015\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00028\u0002H\u0016\u00a2\u0006\u0002\u0010\tJ\b\u0010\n\u001a\u00020\u0007H&\u00a8\u0006\u000b"}, d2 = {"Lcom/example/app/base/BaseAdapter$Holder;", "E", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "item", "(Ljava/lang/Object;)V", "unbind", "app_debug"})
    public static abstract class Holder<E extends java.lang.Object> extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public void bind(E item) {
        }
        
        public abstract void unbind();
        
        public Holder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}