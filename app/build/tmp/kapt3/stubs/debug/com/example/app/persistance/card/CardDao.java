package com.example.app.persistance.card;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\bg\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0005H\'J\u001f\u0010\t\u001a\u00020\u00072\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000b\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\f"}, d2 = {"Lcom/example/app/persistance/card/CardDao;", "", "getCards", "Landroidx/lifecycle/LiveData;", "", "Lcom/example/app/entity/Card;", "insertCard", "", "card", "removeCards", "cardList", "(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public abstract interface CardDao {
    
    @androidx.room.Insert()
    public abstract void insertCard(@org.jetbrains.annotations.NotNull()
    com.example.app.entity.Card card);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from Card")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.example.app.entity.Card>> getCards();
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Delete()
    public abstract java.lang.Object removeCards(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.app.entity.Card> cardList, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1);
}