package com.example.app.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001f\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\u0012\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u000eJ\u000e\u0010\u000f\u001a\u00020\b2\u0006\u0010\u0010\u001a\u00020\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0011"}, d2 = {"Lcom/example/app/repository/CardRepo;", "", "database", "Lcom/example/app/persistance/AppDatabase;", "(Lcom/example/app/persistance/AppDatabase;)V", "getDatabase", "()Lcom/example/app/persistance/AppDatabase;", "deleteCards", "", "cardList", "", "Lcom/example/app/entity/Card;", "(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getCards", "Landroidx/lifecycle/LiveData;", "insertCard", "card", "app_debug"})
public final class CardRepo {
    @org.jetbrains.annotations.NotNull()
    private final com.example.app.persistance.AppDatabase database = null;
    
    public final void insertCard(@org.jetbrains.annotations.NotNull()
    com.example.app.entity.Card card) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.example.app.entity.Card>> getCards() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object deleteCards(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.app.entity.Card> cardList, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.app.persistance.AppDatabase getDatabase() {
        return null;
    }
    
    public CardRepo(@org.jetbrains.annotations.NotNull()
    com.example.app.persistance.AppDatabase database) {
        super();
    }
}