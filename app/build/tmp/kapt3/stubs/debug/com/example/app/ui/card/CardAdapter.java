package com.example.app.ui.card;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u0002\u0012\b\u0012\u00060\u0003R\u00020\u00000\u0001:\u0001\u0015B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u000b\u001a\u00020\fJ\u0006\u0010\r\u001a\u00020\u000eJ\u001c\u0010\u000f\u001a\u00060\u0003R\u00020\u00002\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u000eH\u0016J\u000e\u0010\u0013\u001a\u00020\f2\u0006\u0010\u0014\u001a\u00020\u000e\u00a8\u0006\u0016"}, d2 = {"Lcom/example/app/ui/card/CardAdapter;", "Lcom/example/app/base/BaseAdapter;", "Lcom/example/app/entity/Card;", "Lcom/example/app/ui/card/CardAdapter$ViewHolder;", "context", "Landroid/content/Context;", "itemClickListener", "Lcom/example/app/base/ItemClickListener;", "itemLongClickListener", "Lcom/example/app/base/ItemLongClickListener;", "(Landroid/content/Context;Lcom/example/app/base/ItemClickListener;Lcom/example/app/base/ItemLongClickListener;)V", "clearSelections", "", "getSelectedItemCount", "", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "toggleSelection", "position", "ViewHolder", "app_debug"})
public final class CardAdapter extends com.example.app.base.BaseAdapter<com.example.app.entity.Card, com.example.app.ui.card.CardAdapter.ViewHolder> {
    
    public final int getSelectedItemCount() {
        return 0;
    }
    
    public final void clearSelections() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.example.app.ui.card.CardAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    public final void toggleSelection(int position) {
    }
    
    public CardAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.example.app.base.ItemClickListener itemClickListener, @org.jetbrains.annotations.NotNull()
    com.example.app.base.ItemLongClickListener itemLongClickListener) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0086\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u00032\u00020\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0012\u0010\u000b\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J\u0012\u0010\u000e\u001a\u00020\u000f2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J\b\u0010\u0010\u001a\u00020\tH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lcom/example/app/ui/card/CardAdapter$ViewHolder;", "Lcom/example/app/base/BaseAdapter$Holder;", "Lcom/example/app/entity/Card;", "Landroid/view/View$OnLongClickListener;", "Landroid/view/View$OnClickListener;", "binding", "Lcom/example/app/databinding/CardListItemBinding;", "(Lcom/example/app/ui/card/CardAdapter;Lcom/example/app/databinding/CardListItemBinding;)V", "bind", "", "item", "onClick", "v", "Landroid/view/View;", "onLongClick", "", "unbind", "app_debug"})
    public final class ViewHolder extends com.example.app.base.BaseAdapter.Holder<com.example.app.entity.Card> implements android.view.View.OnLongClickListener, android.view.View.OnClickListener {
        private final com.example.app.databinding.CardListItemBinding binding = null;
        
        @java.lang.Override()
        public void unbind() {
        }
        
        @java.lang.Override()
        public void bind(@org.jetbrains.annotations.NotNull()
        com.example.app.entity.Card item) {
        }
        
        @java.lang.Override()
        public boolean onLongClick(@org.jetbrains.annotations.Nullable()
        android.view.View v) {
            return false;
        }
        
        @java.lang.Override()
        public void onClick(@org.jetbrains.annotations.Nullable()
        android.view.View v) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.example.app.databinding.CardListItemBinding binding) {
            super(null);
        }
    }
}